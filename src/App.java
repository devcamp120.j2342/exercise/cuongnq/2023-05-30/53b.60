import model.Animal;
import model.Cat;
import model.Dog;
import model.Mammal;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Animal animal1 = new Animal("bo");
        Animal animal2 = new Animal("heo");

        System.out.println(animal1.toString());
        System.out.println(animal2.toString());

        Mammal mammal1 = new Mammal("bo", "bo sua");
        Mammal mammal2 = new Mammal("heo", "heo sua");

        System.out.println(mammal1.toString());
        System.out.println(mammal2.toString());

        Cat cat1 = new Cat("bo", "bo sua", "meo2");
        Cat cat2 = new Cat("heo", "heo sua", "meo2");

        System.out.println(cat1.toString());
        System.out.println(cat2.toString());

        Dog dog1 = new Dog("bo", "bo sua", "cho dom");
        Dog dog2 = new Dog("heo", "heo sua", "cho muc");

        System.out.println(dog1.toString());
        System.out.println(dog2.toString());

        cat1.greets();
        cat2.greets();

        dog1.greets();
        dog2.greets();

        dog1.greets(dog2);
    }
}
