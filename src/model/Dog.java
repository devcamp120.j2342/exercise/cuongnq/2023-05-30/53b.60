package model;

public class Dog extends model.Mammal{
    public String name;

    public Dog(String name, String name2, String name3) {
        super(name, name2);
        name = name3;
    }
    
    public void greets() {
        System.out.println("woof");
    }

    public void greets(Dog another) {
        System.out.println("woooof");
    }

    @Override
    public String toString() {
        return "Dog [" + super.toString() + "name=" + this.name + "]";
    }


}
