package model;

public class Mammal extends model.Animal {
    private String name;

    public Mammal(String name, String name2) {
        super(name);
        name = name2;
    }

    @Override
    public String toString() {
        return "Mammal [" + super.toString() + "name=" + this.name + "]";
    }


}
