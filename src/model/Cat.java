package model;

public class Cat extends model.Mammal{
    public String name;

    public Cat(String name, String name2, String name3) {
        super(name, name2);
        name = name3;
    }

    public void greets() {
        System.out.println("meow");
    }

    @Override
    public String toString() {
        return "Cat [" + super.toString() + "name=" + this.name + "]";
    }


}
